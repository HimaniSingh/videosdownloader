package com;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.appzmachine.videodownloader.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.transition.Transition;
import com.utils.Methods;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ViewAdpter extends RecyclerView.Adapter<ViewAdpter.ViewHolder> {

    private Activity activity;
    private int columnWidth;
    private List<File> stringList;
    private String string;
    private Methods method;

    public ViewAdpter(Activity activity, List<File> stringList, String string, Methods method) {
        this.activity = activity;
        this.stringList = stringList;
        this.string = string;
        this.method = method;
        columnWidth = method.getScreenWidth();
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(activity).inflate(R.layout.viewadpter, parent, false);

        return new ViewAdpter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.imageView.setLayoutParams(new ConstraintLayout.LayoutParams(columnWidth / 2, columnWidth / 2));

        Glide.with(activity).load(stringList.get(position).toString())
                .placeholder(R.drawable.ic_launcher_background).into(holder.imageView);

        holder.imageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                method.click(position, string);
            }
        });

    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        public ViewHolder(android.view.View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_view_adapter);

        }
    }
}
