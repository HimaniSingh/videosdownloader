package com.appzmachine.videodownloader;

import android.content.Context;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ImagesFragment;

public class MyAdapter extends FragmentStatePagerAdapter {

    private Context myContext;
    int totalTabs;

    public MyAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                WhatsAppFragment whatsAppFragment = new WhatsAppFragment();
                return whatsAppFragment;
            case 1:
                AddsClass addsClass=new AddsClass(myContext);
                addsClass.showInterstitial();
                AllVideosFragment allVideosFragment = new AllVideosFragment();
                return allVideosFragment;

            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}
