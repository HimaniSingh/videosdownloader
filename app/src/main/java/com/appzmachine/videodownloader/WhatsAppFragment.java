package com.appzmachine.videodownloader;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import java.io.File;
import java.util.ArrayList;


public class WhatsAppFragment extends Fragment {

    RecyclerView recyclerviewwhatsaap;
     String WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses";
    WhatsappAdpter whatsappAdpter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_whats_app, container, false);
        recyclerviewwhatsaap = view.findViewById(R.id.recyclerviewwhatsaap);
        LinearLayoutManager mLinearLayoutManager = new GridLayoutManager(getContext(),2);
        recyclerviewwhatsaap.setLayoutManager(mLinearLayoutManager);
        WhatsappAdpter recyclerViewMediaAdapter = new WhatsappAdpter(getContext(), this.getListFiles(new File(Environment.getExternalStorageDirectory().toString() + WHATSAPP_STATUSES_LOCATION)));
        recyclerviewwhatsaap.setAdapter(recyclerViewMediaAdapter);
        return view;
    }


    private ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {
                Log.e("check", file.getName());
                if (file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".gif") ||
                        file.getName().endsWith(".mp4")) {
                    if (!inFiles.contains(file))
                        inFiles.add(file);
                }
            }
        }
        return inFiles;
    }
}