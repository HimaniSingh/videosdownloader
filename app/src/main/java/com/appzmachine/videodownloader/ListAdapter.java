package com.appzmachine.videodownloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import java.io.File;
import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {
    final Context context;
    final ArrayList<File> modelFeedArrayList;
    private static final String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WhatsappStatus/";

    public ListAdapter(Context context, final ArrayList<File> modelFeedArrayList) {
        this.context = context;
        this.modelFeedArrayList = modelFeedArrayList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_media_row_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        File currentFile = modelFeedArrayList.get(position);

        if (currentFile.getAbsolutePath().endsWith(".mp4")) {
            holder.cardViewImageMedia.setVisibility(View.GONE);
            holder.cardViewVideoMedia.setVisibility(View.VISIBLE);
            Uri video = Uri.parse(currentFile.getAbsolutePath());
            holder.videoViewVideoMedia.setVideoURI(video);
            holder.videoViewVideoMedia.setOnPreparedListener(mp -> {
                mp.setLooping(true);
                holder.videoViewVideoMedia.start();
            });
        } else {
            holder.cardViewImageMedia.setVisibility(View.VISIBLE);
            holder.cardViewVideoMedia.setVisibility(View.GONE);
            Bitmap myBitmap = BitmapFactory.decodeFile(currentFile.getAbsolutePath());
            holder.imageViewImageMedia.setImageBitmap(myBitmap);
        }
    }


    @Override
    public int getItemCount() {
        return modelFeedArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewImageMedia;
        VideoView videoViewVideoMedia;
        CardView cardViewVideoMedia;
        CardView cardViewImageMedia;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewImageMedia = itemView.findViewById(R.id.imageViewImageMedia);
            videoViewVideoMedia = itemView.findViewById(R.id.videoViewVideoMedia);
            cardViewVideoMedia = itemView.findViewById(R.id.cardViewVideoMedia);
            cardViewImageMedia = itemView.findViewById(R.id.cardViewImageMedia);

        }
    }
}
