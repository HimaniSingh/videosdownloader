package com.appzmachine.videodownloader;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
    //private MyListData[] listdata;
    ArrayList<MyListData> listdata;
    File directory;
    String[] AllPath;
    Context context;
    // RecyclerView recyclerView;
    public MyListAdapter(Context context, ArrayList<MyListData>listdata) {
        this.listdata = listdata;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyListData myListData = listdata.get(position);
       // holder.textView.setText(listdata.get(position).getDescription());
       // holder.imageView.setImageResource(listdata[position].getImgId());

         Glide.with(context).load(listdata.get(position).getThum()).skipMemoryCache(false).into(holder.textView);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(view.getContext(), "click on item: " + myListData.getStr_path(), Toast.LENGTH_LONG).show();
                Intent intent=new Intent(context, VideoActivity.class);
                intent.putExtra("video",listdata.get(position).getStr_path());
                intent.putExtra("id",listdata.get(position).getId());
              //  Log.i("kkkk",listdata.get(position).getStr_path());
                context.startActivity(intent);
            }
        });




    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public ImageView textView;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textView = (ImageView) itemView.findViewById(R.id.textView);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);
        }
    }

    public void loadDirectory(File Directory){



    }

}