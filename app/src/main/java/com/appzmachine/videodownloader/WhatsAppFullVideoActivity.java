package com.appzmachine.videodownloader;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.mateware.snacky.Snacky;

public class WhatsAppFullVideoActivity extends AppCompatActivity {
   VideoView videos;
   Uri uri;
   String wvideo;
   String id;
   LinearLayout download,whatsaapshare;
    private static final String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WhatsappStatus/";
    private List<File> showArray;
    private ProgressDialog progressDialog;
    public static final String downloadUrl = "/status/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_app_full_video);
        videos=findViewById(R.id.videos);
        download=findViewById(R.id.download);
        whatsaapshare=findViewById(R.id.whatsaapshare);
        Intent intent = getIntent();
        wvideo = intent.getExtras().getString("whatsappvideos");
        id = intent.getExtras().getString("id");
        showArray = new ArrayList<>();
        Log.i("kkk",wvideo);



        uri= Uri.parse(wvideo);// initiate a video view
        videos.setVideoURI(uri);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videos.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videos.setLayoutParams(params);
        videos.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                videos.start();
            }
        });



        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveVideo(id);

//                Toast.makeText(WhatsAppFullVideoActivity.this, "Download Successfully", Toast.LENGTH_SHORT).show();
//                downloaders();
//                AddsClass addsClass=new AddsClass(WhatsAppFullVideoActivity.this);
//                addsClass.showInterstitial();

            }
        });
        whatsaapshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File f = new File(wvideo);
                Uri uriPath = Uri.parse(f.getPath());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String shareMessage = "\nHi, Download this cool & fast performance Video Downloader App.\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uriPath);
                shareIntent.setType("video/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "send"));

            }
        });


    }


    public void saveVideo(final String str) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String playURL = "";
                final StringBuilder stringBuilder = new StringBuilder();
                try {
                    Document doc = Jsoup.connect(id)
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .post();

                    System.out.println(doc);
                    Elements meta = doc.select("meta[property=og:video]");

                    for (Element src : meta) {
                        if (src.tagName().equals("meta"))
                            Log.d("TAG", " content: " + src.attr("content"));
                        playURL = src.attr("content");

                    }

                   // downloaders(playURL);

                }catch (Exception e){

                }



            }
        }).start();
    }
    public void downloaders(final String str) {
        String storagePath = Environment.getExternalStorageDirectory().getPath() + "/Videos/";
//Log.d("Strorgae in view",""+storagePath);
        File f = new File(storagePath);
        if (!f.exists()) {
            f.mkdirs();
        }
        String pathname = f.toString();
        if (!f.exists()) {
            f.mkdirs();
        }
        DownloadManager dm = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(str);

        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir("VideoDownloader", uri.getLastPathSegment());
        request.setTitle(getString(R.string.app_name));
        Long referese = dm.enqueue(request);

    }


    public View.OnClickListener downloadMediaItem(String sourceFile ) {

        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new Runnable(){

                    @Override
                    public void run() {
                        try {
                            copyFile(new File(sourceFile), new File(Environment.getExternalStorageDirectory().toString() + DIRECTORY_TO_SAVE_MEDIA_NOW ));
                            Snacky.builder().
                                    setActivty(WhatsAppFullVideoActivity.this).
                                    setText("save_successful_message").
                                    success().
                                    show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("RecyclerV", "onClick: Error:"+e.getMessage() );

                            Snacky.builder().
                                    setActivty(WhatsAppFullVideoActivity.this).
                                    setText("save_error_message").
                                    error().
                                    show();
                        }
                    }
                }.run();
            }
        };
    }

    /**
     * copy file to destination.
     *
     * @param sourceFile
     * @param destFile
     * @throws IOException
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }
    private ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {
                Log.e("check", file.getName());
                if (file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".gif") ||
                        file.getName().endsWith(".mp4")) {
                    if (!inFiles.contains(file))
                        inFiles.add(file);
                }
            }
        }
        return inFiles;
    }


    public class Download extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            @SuppressLint("WrongThread")
          //  String file = showArray.get(viewPager.getCurrentItem()).toString();
           // File source = new File(file);
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname, string = null;

            fname = "Video-" + n + ".mp4";
            string = Environment.getExternalStorageDirectory().toString() +downloadUrl + fname;

            File files = new File(string);
            // FileUtils.copyFile(source, files);
            try {
                MediaScannerConnection.scanFile(getApplicationContext(), new String[]{files.toString()},
                        null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(WhatsAppFullVideoActivity.this, getResources().getString(R.string.download), Toast.LENGTH_SHORT).show();
            progressDialog.hide();
            super.onPostExecute(s);
        }
    }


}