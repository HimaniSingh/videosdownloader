package com.appzmachine.videodownloader;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.ShowItemActivity;

import java.io.File;
import java.util.ArrayList;

public class WhatsappAdpter extends RecyclerView.Adapter<WhatsappAdpter.MyViewHolder> {
    final Context context;
    final ArrayList<File> modelFeedArrayList;
    private static final String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WhatsappStatus/";

    public WhatsappAdpter(Context context, final ArrayList<File> modelFeedArrayList) {
        this.context = context;
        this.modelFeedArrayList = modelFeedArrayList;
    }


    @NonNull
    @Override
    public WhatsappAdpter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_media_row_item, parent, false);
        return new WhatsappAdpter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final WhatsappAdpter.MyViewHolder holder, int position) {

        File currentFile = modelFeedArrayList.get(position);

        if (currentFile.getAbsolutePath().endsWith(".mp4")) {
            holder.cardViewImageMedia.setVisibility(View.GONE);
            holder.cardViewVideoMedia.setVisibility(View.VISIBLE);
            Uri video = Uri.parse(currentFile.getAbsolutePath());
          //  holder.videoViewVideoMedia.setVideoURI(video);
            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(currentFile.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
            holder.videoViewVideoMedia.setImageBitmap(bMap);
//            holder.videoViewVideoMedia.setOnPreparedListener(mp -> {
//                mp.setLooping(true);
//                //holder.videoViewVideoMedia.start();
//            });
        } else {
            holder.cardViewImageMedia.setVisibility(View.VISIBLE);
            holder.cardViewVideoMedia.setVisibility(View.GONE);
            Bitmap myBitmap = BitmapFactory.decodeFile(currentFile.getAbsolutePath());
            holder.imageViewImageMedia.setImageBitmap(myBitmap);
        }
        holder.cardViewVideoMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, WhatsAppFullVideoActivity.class);
                intent.putExtra("whatsappvideos",currentFile.getAbsolutePath());
                intent.putExtra("id",currentFile.getName());
                Log.i("lll", String.valueOf(modelFeedArrayList.get(position)));
                Log.i("himooo",currentFile.getAbsolutePath());
                Log.i("himooo1",currentFile.getName());
                context.startActivity(intent);

            }
        });
        holder.cardViewImageMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, WhatsAppImagesActivity.class);
                intent.putExtra("whatsappimage",currentFile.getAbsolutePath());
                intent.putExtra("id",currentFile.getName());
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return modelFeedArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewImageMedia;
        ImageView videoViewVideoMedia;
        CardView cardViewVideoMedia;
        CardView cardViewImageMedia;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewImageMedia = itemView.findViewById(R.id.imageViewImageMedia);
            videoViewVideoMedia = itemView.findViewById(R.id.videoViewVideoMedia);
            cardViewVideoMedia = itemView.findViewById(R.id.cardViewVideoMedia);
            cardViewImageMedia = itemView.findViewById(R.id.cardViewImageMedia);

        }
    }
}
