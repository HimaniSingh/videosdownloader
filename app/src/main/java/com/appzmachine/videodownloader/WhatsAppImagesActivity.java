package com.appzmachine.videodownloader;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.Random;

public class WhatsAppImagesActivity extends AppCompatActivity {
    ImageView imag;
    String im;
    LinearLayout whatsaapshareimage,imagedownload;
    Bitmap myBitmap;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_app_images);
        imag=findViewById(R.id.imag);
        whatsaapshareimage=findViewById(R.id.whatsaapshareimage);
        imagedownload=findViewById(R.id.imagedownload);
        Intent intent = getIntent();
        im = intent.getExtras().getString("whatsappimage");
        id = intent.getExtras().getString("id");
         myBitmap = BitmapFactory.decodeFile(im);
        imag.setImageBitmap(myBitmap);


        whatsaapshareimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File f = new File(im);
                Uri uriPath = Uri.parse(f.getPath());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String shareMessage = "\nHi, Download this cool & fast performance Video Downloader App.\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uriPath);
                shareIntent.setType("video/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "send"));

            }
        });
        imagedownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(WhatsAppImagesActivity.this, "Download Successfully", Toast.LENGTH_SHORT).show();
download();
                //                File f = new File(im);
//                Uri uriPath = Uri.parse(f.getPath());
//                Intent shareIntent = new Intent();
//                shareIntent.setAction(Intent.ACTION_SEND);
//                String shareMessage = "\nHi, Download this cool & fast performance Video Downloader App.\n";
//                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
//                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
//                shareIntent.putExtra(Intent.EXTRA_STREAM, uriPath);
//                shareIntent.setType("video/*");
//                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                startActivity(Intent.createChooser(shareIntent, "send"));

            }
        });

    }
    public void download(){
        File mainfile;
        String fpath;


        try {
//i.e  v2:My view to save on own folder
          //  v2.setDrawingCacheEnabled(true);
//Your final bitmap according to my code.
         //  @SuppressLint("UseCompatLoadingForDrawables")
          // Bitmap bitmap_tmp = getB(Integer.parseInt(id));

            new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)+File.separator+"/VideoDownloader");

            Random random=new Random();
            int ii=100000;
            ii=random.nextInt(ii);
            String fname="MyPic_"+ ii + ".jpg";
            File direct = new File(Environment.getExternalStorageDirectory() + "/MyFolder");

            if (!direct.exists()) {
                File wallpaperDirectory = new File("/sdcard/MyFolder/");
                wallpaperDirectory.mkdirs();
            }

            mainfile = new File(new File("/sdcard/MyFolder/"), fname);
            if (mainfile.exists()) {
                mainfile.delete();
            }

            FileOutputStream fileOutputStream;
            fileOutputStream = new FileOutputStream(mainfile);

           // bitmap_tmp.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            Toast.makeText(WhatsAppImagesActivity.this.getApplicationContext(), "Saved in Gallery..", Toast.LENGTH_LONG).show();
            fileOutputStream.flush();
            fileOutputStream.close();
            fpath=mainfile.toString();
            galleryAddPic(fpath);
        } catch(FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void galleryAddPic(String fpath) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(fpath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}