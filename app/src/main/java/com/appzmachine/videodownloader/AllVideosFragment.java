package com.appzmachine.videodownloader;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;




import java.util.ArrayList;


public class AllVideosFragment extends Fragment  {


    ArrayList<MyListData> arryList;
    RecyclerView recyclerView;
    String absolutpath=null;
    int column_index_data,thum;
    MyListAdapter myListAdapter;

    MyListData myListData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_all_videos, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        init();

        return view;
    }


    public void init(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        arryList=new ArrayList<>();
        getAllMedia();

    }

    @SuppressLint("Recycle")
    public void getAllMedia() {
        String st;
        Uri uri;
        Cursor videocursor = null;
        uri= MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        st=uri.getLastPathSegment();
        // String[] selection= new String[]{"Videos"};
//        String selection=MediaStore.Video.Media.DATA +" like?";
//
//        String[] selectionArgs = new String[]{Environment.getExternalStorageDirectory().getPath()+ "/Videos/"};

        String []projection= new String[0];
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            projection = new String[]{Environment.getExternalStorageDirectory().getPath()+ "/Videos/"};
            Log.i("himanisingh", String.valueOf(projection));
        }
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
//            String orderby = MediaStore.Images.Media.DISPLAY_NAME;
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            videocursor= Objects.requireNonNull(getContext()).getContentResolver().query(uri,projection,selection,selectionArgs);
//        }

        String selection=MediaStore.Video.Media.DATA +" like?";
        String[] selectionArgs=new String[]{"%VideoDownloader%"};
        videocursor = getActivity().managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, selection, selectionArgs, MediaStore.Video.Media.DATE_TAKEN );


        column_index_data=videocursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
     //   Log.i("himmooo", String.valueOf(column_index_data));
        thum=videocursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);
        while (videocursor.moveToNext()){
            absolutpath=videocursor.getString(column_index_data);
             myListData=new MyListData();
            myListData.setSelected(false);
            myListData.setStr_path(absolutpath);
            myListData.setThum(videocursor.getString(thum));
            arryList.add(myListData);
            myListAdapter= new MyListAdapter(getContext(),arryList);
            recyclerView.setAdapter(myListAdapter);

        }

    }

}