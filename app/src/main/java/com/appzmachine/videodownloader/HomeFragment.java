package com.appzmachine.videodownloader;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.CLIPBOARD_SERVICE;
import static android.content.Context.DOWNLOAD_SERVICE;
import static android.os.Environment.getExternalStorageDirectory;


public class HomeFragment extends Fragment {
    Button button;
    File fos;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    EditText pasteedttxt;
    ProgressDialog progressDialog;

    ImageView mojimg, respo, fb,whatsaap;
    String url = "http://www.youtube.com/watch?v=qvtCk1wZ7LM&feature=player_detailpage";
    String getUrl = "";
    Button pastebtn;
    ProgressDialog mProgressDialog;
    AdView adview;

    private DownloadManager downloadManager;
    ArrayList<String> trueLink;
    // String fileName = "https://mojapp.in/@ravikira_33/video/106204006?referrer=share";
    // String fileName = "https://www.facebook.com/tseriesmusic/videos/2092418317721410/";
    String fileName = "https://www.roposo.com/story/auto-original-hindi-english-backend_glanceworthy-channel_beats-channel_roposostars-varshachaudhary-roposobeats/d7f64eb0-1e0c-4a56-8134-b8b4ecfbfe29";

    private static final String webSiteURL = "http://www.supercars.net/gallery/119513/2841/5.html";

    //The path of the folder that you want to save the images to
    private static final String folderPath = "<FOLDER PATH>";
    String str;
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private long fileSize = 0;
    String playURL = "";
    String p;
    String paste;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        button = view.findViewById(R.id.button);
        mojimg = view.findViewById(R.id.mojimg);
        fb = view.findViewById(R.id.fb);
        whatsaap = view.findViewById(R.id.whatsaap);
        pasteedttxt = view.findViewById(R.id.pasteedttxt);
        respo = view.findViewById(R.id.respo);
        pastebtn = view.findViewById(R.id.pastebtn);
        requestPermission();

        adview = view.findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adview.loadAd(adRequest);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUrl = pasteedttxt.getText().toString();
                if (getUrl.equals("")) {
                    Toast.makeText(getContext(), "Please Paste URL", Toast.LENGTH_SHORT).show();
                } else {
                    trueLink = getURLS(getUrl);
                   // saveVideo(trueLink.get(0));
                    new DownloadFileAsync().execute(trueLink.get(0));



                }

            }//end of onClick method
        });





        pastebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(CLIPBOARD_SERVICE);



                    ClipData pData = clipboard.getPrimaryClip();
                    ClipData.Item item = pData.getItemAt(0);

                if (!item.equals("")){
                    Toast.makeText(getContext(), "First Copy Text", Toast.LENGTH_SHORT).show();
                }else{

                    String txtpaste = item.getText().toString();
                    pasteedttxt.setText(txtpaste);
                    Toast.makeText(getContext(),"Paste",Toast.LENGTH_SHORT).show();

                }


            }
        });
        mojimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isAppInstalled = appInstalledOrNot("in.mohalla.video");

                if(isAppInstalled) {
                    //This intent will help you to launch if the package is already installed
                    Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("in.mohalla.video");
                    startActivity(launchIntent);
                } else {
                    // Do whatever we want to do if application not installed
                    // For example, Redirect to play store
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "in.mohalla.video")));

                }


            }
        });
        respo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isAppInstalled = appInstalledOrNot("com.roposo.android");

                if(isAppInstalled) {
                    //This intent will help you to launch if the package is already installed
                    Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.roposo.android");
                    startActivity(launchIntent);
                } else {
                    // Do whatever we want to do if application not installed
                    // For example, Redirect to play store
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.roposo.android")));

                }

            }
        });
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isAppInstalled = appInstalledOrNot("com.facebook.katana");

                if(isAppInstalled) {
                    //This intent will help you to launch if the package is already installed
                    Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
                    startActivity(launchIntent);
                } else {
                    // Do whatever we want to do if application not installed
                    // For example, Redirect to play store
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.facebook.katana")));

                }

            }
        });
        whatsaap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isAppInstalled = appInstalledOrNot("com.whatsapp");

                if(isAppInstalled) {
                    //This intent will help you to launch if the package is already installed
                    Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                    startActivity(launchIntent);
                } else {
                    // Do whatever we want to do if application not installed
                    // For example, Redirect to play store
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.whatsapp")));

                }

            }
        });
        return view;
    }


    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat
                    .requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void startLoding() {

        // st=pasteedttxt.getText().toString().trim();
        String PATH = getExternalStorageDirectory().toString()
                + "/load";
        Log.v("LOG_TAG", "PATH: " + PATH);

//        storage/emulated/0/load


//
//
//       // final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(withoutWatermark()));
//
//        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileName));
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
//        request.setTitle("Downloading");
//        request.setDescription("Downloading file");
//        request.allowScanningByMediaScanner();
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "" + System.currentTimeMillis());
//        DownloadManager manager = (DownloadManager) getContext().getSystemService(getContext().DOWNLOAD_SERVICE);
//        manager.enqueue(request);


    }

    @NonNull
    public final ArrayList<String> getURLS(final String str) {
        final ArrayList<String> arrayList = new ArrayList<>();
        final Matcher matcher = Pattern.compile("\\(?\\b(https?://|www[.]|ftp://)[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]").matcher(str);

        while (matcher.find()) {
            String group = matcher.group();
            if (group.startsWith("(") && group.endsWith(")"))
                group = group.substring(1, group.length() - 1);
            arrayList.add(group);
        }
        return arrayList;
    }

    /**
     * This is where the magic happens, first we spoof our device using Jsoup than we process the url and lastly we get the playurl
     */
    public void saveVideo(final String str) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                // final StringBuilder stringBuilder = new StringBuilder();
                try {
                    Document doc = Jsoup.connect(str)
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .method(Connection.Method.POST)
                            .get();


                    System.out.println(doc);

                    Elements meta = doc.select("meta[property=og:video]");
                    for (Element src : meta) {
                        if (src.tagName().equals("meta"))
                            Log.d("TAG", " content: " + src.attr("content"));
                        playURL = src.attr("content");

                    }


                    //                  Elements element=doc.select("meta");
//                    Log.d("TAG", " content: " + element.attr("content"));

//                    Elements src = doc.select("meta");
//                    for (Element urls : element) {
//                        //System.out.println(urls.text());
//                        Log.d("TAG", " url is: " + urls.attr("property") + " videos" + urls.attr("og:video"));
//                    }

//
//                    for (Element src : element) {
//                        if (src.text().equals("og:video"))
//                            Log.d("TAG", " content: " + src.attr("content"));
//                           // Log.d("TAG", " content: " + src.attr("og:video"));
//                        else
//                            Log.d("TAG", src.tagName());
//
//                    }


//


//                    Document doc = Jsoup.connect(str).get();
//
//                    System.out.println(doc);
//                    Elements vid = doc.select("meta");
//                   System.out.println("\nlink: " + vid.attr("content"));
//
//
//                    System.out.println("text: " + vid.text());
//                    Log.i("himani",vid.text());
//                    playURL=vid.attr("href");
//
//                    for (Element element : doc.select("video")) {
//                        final String data = element.data();
//                        if (data.contains("videodata")) {
//                            final String substring = data.substring(data.lastIndexOf("name"));
//                            final String substring2 = substring.substring(substring.indexOf("[") + 1);
//                            final String substring3 = substring2.substring(0, substring2.indexOf("]"));
//                            playURL = substring3.substring(1, substring3.length() - 1);
//
//                            Log.i("HIMANISINGH",substring+substring2+substring3);
//                        }
//                    }


                    new DownloadFileAsync().execute(playURL);

                } catch (Exception e) {

                }


            }
        }).start();
    }


    public void downloader(final String str) {

        String storagePath = Environment.getExternalStorageDirectory().getPath() + "/Videos/";
//Log.d("Strorgae in view",""+storagePath);
        File f = new File(storagePath);
        if (!f.exists()) {
            f.mkdirs();
        }
        String pathname = f.toString();
        if (!f.exists()) {
            f.mkdirs();
        }
        DownloadManager dm = (DownloadManager) getContext().getSystemService(DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(str);

        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir("VideoDownloader", uri.getLastPathSegment());
        request.setTitle(getString(R.string.app_name));
        Long referese = dm.enqueue(request);


    }

    private static void getImages(String src) throws IOException {

        String folder = null;

        //Exctract the name of the image from the src attribute
        int indexname = src.lastIndexOf("/");

        if (indexname == src.length()) {
            src = src.substring(1, indexname);
        }

        indexname = src.lastIndexOf("/");
        String name = src.substring(indexname, src.length());

        System.out.println(name);

        //Open a URL Stream
        URL url = new URL(src);
        InputStream in = url.openStream();

        OutputStream out = new BufferedOutputStream(new FileOutputStream(folderPath + name));

        for (int b; (b = in.read()) != -1; ) {
            out.write(b);
        }
        out.close();
        in.close();

    }

    public String withoutWatermark(final String url) {
        try {

            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            String PATH = Environment.getExternalStorageDirectory().toString()
                    + "/load";
            Log.v("LOG_TAG", "PATH: " + PATH);

            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(String.valueOf(file));
            FileOutputStream fos = new FileOutputStream(outputFile);
            InputStream is = c.getInputStream();

            byte[] buffer = new byte[4096];
            int len1 = 0;

            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }

            fos.close();
            is.close();

            Toast.makeText(getContext(), " A new file is downloaded successfully",
                    Toast.LENGTH_LONG).show();
//            String rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                    +"video";
//            String PATH = Environment.getExternalStorageDirectory().toString()
//                    + "/load";
//            Log.v("LOG_TAG", "PATH: " + PATH);
//           // String rootDir = getExternalStorageDirectory().getAbsolutePath();
//           // String targetPath = rootDir + "/DCIM/Video";
//            File rootFile = new File(PATH);
//            rootFile.mkdir();
//            URL u = new URL(url);
//            HttpURLConnection c = (HttpURLConnection) u.openConnection();
//            c.setRequestMethod("GET");
//            c.setDoOutput(true);
//            c.connect();
//            FileOutputStream f = new FileOutputStream(new File(rootFile,fileName));
//            InputStream in = c.getInputStream();
//            byte[] buffer = new byte[1024];
//            int len1 = 0;
//            while ((len1 = in.read(buffer)) > 0) {
//                f.write(buffer, 0, len1);
//            }
//            f.close();
        } catch (IOException e) {
            Log.d("Error", e.toString());
        }

        return "";
    }

    private String downloadFile(String fileURL) {
        SimpleDateFormat sd = new SimpleDateFormat("yymmhh");
        String date = sd.format(new Date());
        // String name = "video" + date + ".mp4";

        try {
            String rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    + File.separator + "Videos";
            File rootFile = new File(rootDir);
            rootFile.mkdir();
            URL url = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream f = new FileOutputStream(new File(String.valueOf(rootFile)));
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (IOException e) {
            Log.d("Error....", e.toString());
        }


        return null;
    }


    class DownloadFileAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            try {
                Document doc = Jsoup.connect(getUrl)
                        .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                        .method(Connection.Method.POST)
                        .get();


                System.out.println(doc);

                Elements meta = doc.select("meta[property=og:video]");
                for (Element src : meta) {
                    if (src.tagName().equals("meta"))
                        Log.d("TAG", " content: " + src.attr("content"));
                    playURL = src.attr("content");

                }

                downloader(playURL);


            } catch (Exception e) {

            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(getContext());
            progressBar.setCancelable(true);
            progressBar.setMessage("File downloading ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            //reset progress bar and filesize status
            progressBarStatus = 0;
            fileSize = 0;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

        }


        @Override
        protected void onPostExecute(String unused) {
            //dismiss the dialog after the file was downloaded
            //  dismissDialog(DIALOG_DOWNLOAD_PROGRESS);

            new Thread(new Runnable() {
                public void run() {
                    while (progressBarStatus < 100) {
                        // performing operation
                        progressBarStatus = doOperation();
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // Updating the progress bar
                        progressBarHandler.post(new Runnable() {
                            public void run() {
                                progressBar.setProgress(progressBarStatus);
                            }
                        });
                    }
                    // performing operation if file is downloaded,
                    if (progressBarStatus >= 100) {
                        // sleeping for 1 second after operation completed
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        progressBar.dismiss();



                        // close the progress bar dialog
                    }
                }
            }).start();
            Toast.makeText(getContext(), "Download Successfully", Toast.LENGTH_SHORT).show();


        }
    }

    public int doOperation() {
        //The range of ProgressDialog starts from 0 to 10000
        while (fileSize <= 10000) {
            fileSize++;
            if (fileSize == 1000) {
                return 10;
            } else if (fileSize == 2000) {
                return 20;
            } else if (fileSize == 3000) {
                return 30;
            } else if (fileSize == 4000) {
                return 40; // you can add more else if
            }
         /* else {
                return 100;
            }*/
        }//end of while
        return 100;
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getContext().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
}

