package com.appzmachine.videodownloader;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import de.mateware.snacky.Snacky;

public class VideoActivity extends AppCompatActivity {
VideoView videoview;
    String videostring="";
    String allid="";
    Uri uri;
    private ArrayList<File> filesList;

    private MediaController mediaController;
    LinearLayout share,delete;
    private static final String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WhatsappStatus/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        videoview=findViewById(R.id.videoview);
        share=findViewById(R.id.share);
        delete=findViewById(R.id.delete);
        Intent intent = getIntent();
         videostring = intent.getExtras().getString("video");
         allid = intent.getExtras().getString("id");
        uri=Uri.parse(videostring);// initiate a video view
        videoview.setVideoURI(uri);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoview.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoview.setLayoutParams(params);
        videoview.requestFocus();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                videoview.start();
            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File f = new File(videostring);
                Uri uriPath = Uri.parse(f.getPath());

                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String shareMessage = "\nHi, Download this cool & fast performance Video Downloader App.\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uriPath);
                shareIntent.setType("video/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "send"));
            }
        });



//        delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(VideoActivity.this, "Delete Successfully", Toast.LENGTH_SHORT).show();
//                finish();
//                AddsClass addsClass=new AddsClass(VideoActivity.this);
//                addsClass.showInterstitial();
//
////                File fdelete = new File(videostring);
////                if (fdelete.exists()) {
////                    if (fdelete.delete()) {
////                        System.out.println("file Deleted :" + uri.getPath());
////                    } else {
////                        System.out.println("file not Deleted :" + uri.getPath());
////                    }
////                }
//
//            }
//        });




    }




}