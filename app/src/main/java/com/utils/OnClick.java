package com.utils;

import java.io.Serializable;

public interface OnClick extends Serializable {

    void position(int position, String type);
}
