package com;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import com.appzmachine.videodownloader.BuildConfig;
import com.appzmachine.videodownloader.R;
import com.appzmachine.videodownloader.WhatsappAdpter;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.textview.MaterialTextView;
import com.utils.Constant;
import com.utils.Methods;
import com.utils.OnClick;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ImagesFragment extends Fragment {

    private Methods method;
    private OnClick onClick;
    private String root;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private ViewAdpter viewAdapter;
    private MaterialTextView textView_noData;
    private LayoutAnimationController animation;
    public static final String downloadUrl = "/status/";
    public static final String url = "/WhatsApp/Media/.Statuses";
    public static final String url_second = "/WhatsApp Business/Media/.Statuses";
    String WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses";
    ArrayList<File> getListFiles=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_images, container, false);
        onClick = new OnClick() {
            @Override
            public void position(int position, String type) {
                startActivity(new Intent(getActivity(), ShowItemActivity.class)
                        .putExtra("position", position)
                        .putExtra("type", type));
                Log.i("jhgf",position+type);
            }
        };
        method = new Methods(getActivity(), onClick);

        progressBar = view.findViewById(R.id.progressbar_fragment);
        recyclerView = view.findViewById(R.id.recyclerView_fragment);
        textView_noData = view.findViewById(R.id.textView_noData_fragment);
        textView_noData.setVisibility(View.GONE);

//        int resId = R.anim.layout_animation_from_bottom;
//        animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);

//        recyclerView.setHasFixedSize(true);
//        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
//        recyclerView.setLayoutManager(layoutManager);

        LinearLayoutManager mLinearLayoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        //WhatsappAdpter recyclerViewMediaAdapter = new WhatsappAdpter(getContext(), this.getListFiles(new File(Environment.getExternalStorageDirectory().toString() + WHATSAPP_STATUSES_LOCATION)));
       // recyclerView.setAdapter(recyclerViewMediaAdapter);

        new DataImage().execute();
        return  view;
    }
    @SuppressLint("StaticFieldLeak")
    public class DataImage extends AsyncTask<String, String, String> {

        boolean isVlaue = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                switch (method.url_type()) {
                    case "w": {
                        root = Environment.getExternalStorageDirectory() + url;
                        File file = new File(root);
                        Constant.imageArray.clear();
                        Constant.imageArray = getListFiles(file);
                        break;
                    }
                    case "wb": {
                        root = Environment.getExternalStorageDirectory() + url_second;
                        File file = new File(root);
                        Constant.imageArray.clear();
                        Constant.imageArray = getListFiles(file);
                        break;
                    }
                    case "wball": {
                        root = Environment.getExternalStorageDirectory() + url;
                        File file = new File(root);
                        Constant.imageArray.clear();
                        Constant.imageArray = getListFiles(file);
                        root = Environment.getExternalStorageDirectory() + url_second;
                        File file_second = new File(root);
                        Constant.imageArray.addAll(getListFiles(file_second));
                        break;
                    }

                }
            } catch (Exception e) {
                Log.d("error", e.toString());
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (!isVlaue) {
                if (Constant.imageArray.size() == 0) {
                    textView_noData.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                } else {
                    textView_noData.setVisibility(View.GONE);
                    viewAdapter = new ViewAdpter(getActivity(), Constant.imageArray, "image", method);
                    recyclerView.setAdapter(viewAdapter);
                    recyclerView.setLayoutAnimation(animation);
                }
            } else {
                textView_noData.setVisibility(View.VISIBLE);
            }
            progressBar.setVisibility(View.GONE);

            super.onPostExecute(s);
        }
    }

//    private List<File> getListFiles(File parentDir) {
//        List<File> inFiles = new ArrayList<>();
//        try {
//            Queue<File> files = new LinkedList<>(Arrays.asList(parentDir.listFiles()));
//            while (!files.isEmpty()) {
//                File file = files.remove();
//                if (file.isDirectory()) {
//                    files.addAll(Arrays.asList(file.listFiles()));
//                } else if (file.getName().endsWith(".jpg") || file.getName().endsWith(".gif")) {
//                    inFiles.add(file);
//                }
//            }
//        } catch (Exception e) {
//            Log.d("error", e.toString());
//        }
//        return inFiles;
//    }

    private ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {
                Log.e("check", file.getName());
                if (file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".gif") ||
                        file.getName().endsWith(".mp4")) {
                    if (!inFiles.contains(file))
                        inFiles.add(file);
                }
            }
        }
        return inFiles;
    }

}